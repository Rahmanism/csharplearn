﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace csvtest
{
    class Program
    {
        static void Main(string[] args)
        {
            var reader = new StreamReader("2.csv");

            var hashes = new Dictionary<string, string>();
            for (int i = 0; i < 10000; i++) {
                hashes.Add(Sha256(i.ToString()), i.ToString());
            }

            var users = new Dictionary<string, string>();
            while (!reader.EndOfStream) {
                var values = reader.ReadLine().Split(',');

                users.Add(values[0], values[1]);
            }

            foreach(KeyValuePair<string, string> h in users) {
                string pass = hashes.FirstOrDefault(x => x.Key == h.Value).Value;
                Console.WriteLine($"password of {h.Key} is {pass}");
            }
        }

        static string Sha256(string data)
        {
            using ( var hash = SHA256.Create() ) {
                byte[] bytes = hash.ComputeHash(Encoding.UTF8.GetBytes(data));

                // convert bytes to string
                string result = String.Empty;
                foreach (byte b in bytes) {
                    result += b.ToString("x2");
                }

                return result;
            }
        }
    }
}
